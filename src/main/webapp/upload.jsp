<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="main.css">
  <title>Jakub Jędrych - Doc files saver</title>
</head>
<body>
<div id="form_div">
  <h1>Formularz przesyłania plików</h1>
  <form action="upload-servlet" method="post" enctype="multipart/form-data" onsubmit="return validateExt();">
    <input type="file" name="file" id="file" required><br><br>
    Opis pliku:<input type="text" name="desc" id="desc_in"><br>
    <input type="submit" value="Prześlij plik">
  </form>
<script>
  function validateExt() {
    var fileInput = document.getElementById("file");
    var filePath = fileInput.value;
    var allowedExtensions = /.doc$/i;

    if (!allowedExtensions.exec(filePath)) {
      alert("Niewłaściwe rozszerzenie pliku.");
      fileInput.value = "";
      return false;
    }
    else {
      return true;
    }
  }
</script>
</body>
</html>
