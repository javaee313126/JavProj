<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <title>Strona Główna</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<h1>Witaj w aplikacji przechowującej pliki ".doc"!</h1>

<h2>Autor aplikacji:</h2>
<p>Jakub Jędrych WCY20KC1S0</p>

<h2>Menu:</h2>
<ul>
    <li><a href="upload.jsp">Prześlij plik na serwer</a></li>
    <li><a href="${pageContext.request.contextPath}/list-servlet">Wyświetl pliki</a></li>
</ul>
</body>
</html>
