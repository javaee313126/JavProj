<%@ page import="entity.Document" %>
<%@ page import="javax.print.Doc" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
  <title>Lista plików</title>
  <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<h1>Lista plików:</h1>
<ul>
  <% for (Document file : (List<Document>) request.getAttribute("fileList")) { %>
  <li>
    <a href="download?fileId=<%= file.getId() %>"><%= file.getFilename() %></a>
    - <a href="delete?fileId=<%= file.getId() %>">Usuń plik </a>
    - <a href="#" onclick="showDescription('<%= file.getFileDesc() %>')">Wyświetl opis</a>
  </li>
  <% } %>
</ul>

<div id="descriptionModal" style="display: none;">
  <div id="descriptionContent"></div>
</div>

<script>
  function showDescription(description) {
    document.getElementById("descriptionContent").innerText = description;
    document.getElementById("descriptionModal").style.display = "block";
  }
</script>
</body>
</html>
