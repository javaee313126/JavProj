package wcy20kc1s0.jakubjedrych.javproj;

import EJB.AppController;
import entity.Document;
import jakarta.ejb.EJB;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@WebServlet(name = "downloadServlet", value = "/download")
public class DownloadServlet extends HttpServlet {

    @EJB
    private AppController cont;
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fileId = request.getParameter("fileId");

        cont = new AppController();
        // Pobierz plik z bazy danych na podstawie fileId
        Document fileEntity = cont.getFileFromDatabase(Integer.parseInt(fileId));

        if (fileEntity != null) {
            // Ustaw nagłówki odpowiedzi
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileEntity.getFilename() + "\"");

            // Pobierz dane pliku
            byte[] fileData = fileEntity.getFileData();
            InputStream inputStream = new ByteArrayInputStream(fileData);

            // Uzyskaj strumień wyjściowy odpowiedzi
            OutputStream outputStream = response.getOutputStream();

            // Skopiuj dane pliku do strumienia wyjściowego
            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            // Zamknij strumienie
            inputStream.close();
            outputStream.close();
        } else {
            // Obsłuż sytuację, gdy plik nie istnieje
            response.getWriter().println("Error");
        }
    }
}
