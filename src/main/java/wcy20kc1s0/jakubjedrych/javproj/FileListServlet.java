package wcy20kc1s0.jakubjedrych.javproj;

import EJB.AppController;
import entity.Document;
import jakarta.ejb.EJB;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "filelistServlet", value = "/list-servlet")
public class FileListServlet extends HttpServlet {

    @EJB
    private AppController cont;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        cont = new AppController();
        List<Document> fileList = cont.getFileListFromDatabase();
        request.setAttribute("fileList", fileList);
        request.getRequestDispatcher("fileList.jsp").forward(request, response);
    }
}
