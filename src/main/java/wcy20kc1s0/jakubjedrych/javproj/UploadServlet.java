package wcy20kc1s0.jakubjedrych.javproj;

import java.io.*;
import java.util.List;

import EJB.AppController;
import entity.Document;
import jakarta.ejb.EJB;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/upload-servlet")
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024, // 1 MB
        maxFileSize = 1024 * 1024 * 10,      // 10 MB
        maxRequestSize = 1024 * 1024 * 100   // 100 MB
)
public class UploadServlet extends HttpServlet {
    private String message;
    @EJB
    private AppController cont;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("index.jsp");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

       try{
        Part filePart = request.getPart("file");
        cont = new AppController();
        String fileName = filePart.getSubmittedFileName();
        if(!fileName.endsWith(".doc")){
            throw new ServletException();
        }
        InputStream fileContent = filePart.getInputStream();
        byte[] fileData = new byte[fileContent.available()];
        fileContent.read(fileData);
        String fileDesc = request.getParameter("desc");

        cont.saveFileToDatabase(fileName, fileData, fileDesc);
       }catch (Exception e){
           response.getWriter().println("Zły plik!");
       }
       response.sendRedirect("index.jsp");
    }

    public void destroy() {
    }
}