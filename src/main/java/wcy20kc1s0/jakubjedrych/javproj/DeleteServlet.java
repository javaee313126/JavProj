package wcy20kc1s0.jakubjedrych.javproj;

import entity.Document;
import jakarta.ejb.EJB;
import EJB.AppController;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "deleteServlet", value = "/delete")
public class DeleteServlet extends HttpServlet {

    @EJB
    private AppController cont;
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String fileId = request.getParameter("fileId");
        cont = new AppController();

        if (fileId != null) {
            cont.deleteFileFromDatabase(Integer.parseInt(fileId));
        } else {
            response.getWriter().println("Wystąpił błąd!");
        }
        response.sendRedirect("index.jsp");
    }
}
