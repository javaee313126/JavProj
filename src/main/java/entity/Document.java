package entity;

import jakarta.persistence.*;

@Entity
@Table(name = "WCY20KC1S0_Jedrych_Jakub")
public class Document {
    @Id
    @GeneratedValue
    int id;
    String filename;
    String fileDesc;
    @Lob
    @Column(name = "fileData", columnDefinition = "longblob")
    byte[] fileData;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFileDesc() {
        return fileDesc;
    }

    public void setFileDesc(String fileDesc) {
        if(fileDesc.isBlank())
        {
            this.fileDesc = "Brak opisu.";
        }else {
            this.fileDesc = fileDesc;
        }
    }

    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }



    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
