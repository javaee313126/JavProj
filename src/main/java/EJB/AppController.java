package EJB;

import entity.Document;
import jakarta.ejb.Stateless;
import jakarta.persistence.*;

import java.util.List;

@Stateless
public class AppController {
    EntityManagerFactory emf;
    EntityManager em;

    public void saveFileToDatabase(String fileName, byte[] fileData, String fileDesc) {
        Document doc = new Document();
        doc.setFileData(fileData);
        doc.setFileDesc(fileDesc);
        doc.setFilename(fileName);

        emf = Persistence.createEntityManagerFactory("default");
        em  = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(doc);
        em.getTransaction().commit();
        em.close();
        emf.close();
    }


    public Document getFileFromDatabase(int fileId) {
        emf = Persistence.createEntityManagerFactory("default");
        em  = emf.createEntityManager();
        Query query = em.createQuery("SELECT f FROM Document f WHERE f.id = :fileId");
        query.setParameter("fileId", fileId);
        Document fileEntity = (Document) query.getSingleResult();
        return fileEntity;
    }

    public void deleteFileFromDatabase(int fileId) {
        emf = Persistence.createEntityManagerFactory("default");
        em  = emf.createEntityManager();
        Document file = em.find(Document.class, fileId);

        if (file != null) {
            em.getTransaction().begin();
            em.remove(file);
            em.getTransaction().commit();
            em.close();
            emf.close();
            System.out.println(fileId);
        }
    }

    public List<Document> getFileListFromDatabase(){
        emf = Persistence.createEntityManagerFactory("default");
        em  = emf.createEntityManager();
        String jpql = "SELECT f FROM Document f";
        TypedQuery<Document> query = em.createQuery(jpql, Document.class);
        return query.getResultList();
    }
}
